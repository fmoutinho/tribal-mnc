package com.social.entities;

import org.springframework.context.annotation.Scope;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="Persona")
@Scope("session")
public class Persona implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id ;
    /**
     * Description of the property email.
     */
    @Column(unique = true)
    private String cedula ;
    private String nombres;
    private String apellidos;
    @Column(name="horas_por_dia")
    private int horasPorDia;
    @Column(name="dias_trabajados")
    private int diasTrabajados;

    public Persona() {
    }

    public Persona(String cedula, String nombres, String apellidos, int horasPorDia, int diasTrabajados) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.horasPorDia = horasPorDia;
        this.diasTrabajados = diasTrabajados;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getHorasPorDia() {
        return horasPorDia;
    }

    public void setHorasPorDia(int horasPorDia) {
        this.horasPorDia = horasPorDia;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
}