package com.social.entities;

import org.springframework.context.annotation.Scope;

import javax.persistence.*;

@Entity
@Table(name="restaurant")
@Scope("session")
public class Restaurante {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id ;
    private String rif;
    private String razon_social;
    private String ubicacion;
    private String comentarios;
    //private image logo;

    public Restaurante() {
    }

    public Restaurante(Long id, String rif, String razon_social, String ubicacion, String comentarios) {
        this.id = id;
        this.rif = rif;
        this.razon_social = razon_social;
        this.ubicacion = ubicacion;
        this.comentarios = comentarios;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}
