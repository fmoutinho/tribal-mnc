package com.social.services;

import com.social.dao.PersonaRepository;
import com.social.entities.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Fernando Moutinho
 *
 */
@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;
    public Persona save(Persona persona) {
        return personaRepository.saveAndFlush(persona);
    }

    public Persona update(Persona persona) {
        return personaRepository.save(persona);
    }

    public Persona find(String cedula) {
        return personaRepository.findOneByCedula(cedula);
    }

    public Persona find(Long id) {
        return personaRepository.findOne(id);
    }

    public List<Persona> findAll(){return personaRepository.findAll();}
}