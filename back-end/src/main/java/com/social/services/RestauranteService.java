package com.social.services;

import com.social.dao.PersonaRepository;
import com.social.dao.RestauranteRepositorio;
import com.social.entities.Persona;
import com.social.entities.Restaurante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestauranteService {

    @Autowired
    RestauranteRepositorio restauranteRepositorio;

    public Restaurante save(Restaurante restaurante) {
        return restauranteRepositorio.saveAndFlush(restaurante);
    }
    public Restaurante update(Restaurante restaurante) {
        return restauranteRepositorio.save(restaurante);
    }

    public Restaurante find(Long id) {
        return restauranteRepositorio.findOne(id);
    }

    public List<Restaurante> findAll(){return restauranteRepositorio.findAll();}

}
