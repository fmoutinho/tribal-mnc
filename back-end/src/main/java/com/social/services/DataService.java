package com.social.services;


import com.social.dao.DataRepository;
import com.social.entities.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataService {
    @Autowired
    DataRepository dataRepository;
    public Data save(Data data) {
        return dataRepository.saveAndFlush(data);
    }

    public Data update(Data data) {
        return dataRepository.save(data);
    }

    public Data find(String cedula) {
        return dataRepository.findOneByCedula(cedula);
    }

    public Data find(Long id) {
        return dataRepository.findOne(id);
    }
}