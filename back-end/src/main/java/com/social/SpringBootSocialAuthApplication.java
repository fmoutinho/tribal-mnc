package com.social;

import com.social.services.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * 
 * @author Fernando Moutinho
 *
 */

@SpringBootApplication
public class SpringBootSocialAuthApplication implements  CommandLineRunner {
	@Resource
	StorageService storageService;
	public static void main(String[] args) {
		SpringApplication.run(SpringBootSocialAuthApplication.class, args);
	}
	@Override
	public void run(String... arg) throws Exception {
		storageService.deleteAll();
		storageService.init();
	}
}
