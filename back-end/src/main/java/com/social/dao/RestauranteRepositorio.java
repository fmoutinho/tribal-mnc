package com.social.dao;

import com.social.entities.Persona;
import com.social.entities.Restaurante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestauranteRepositorio extends JpaRepository<Restaurante, Long> {
    public Restaurante findBy(String rif);
    public List<Restaurante> findAll();
}
