package com.social.controller;

//PAQUETES A IMPORTAR

import com.social.entities.Data;
import com.social.entities.Persona;
import com.social.services.DataService;
import com.social.services.PersonaService;
import com.social.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import sun.misc.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
@RequestMapping("account")
public class FileController {
    @Autowired
    PersonaService personaService;
    @Autowired
    DataService dataService;
    @Autowired
    StorageService storageService;
    List<String> fileNames = new ArrayList<String>();
    int lineaElemento=0;

    @CrossOrigin(maxAge = 3600)
    @GetMapping(path = "/actividades/", produces = "application/json;charset=UTF-8")
    public Persona getPersonaProceso(@RequestParam(value = "cedula", required = false) String cedula ) {
        try {
            Persona persona ;
            persona = personaService.find(cedula.toString().trim());
            Data data = new Data();
            data.setCedula(persona.getCedula());
            data.setNumeroDiasTrabajo(Integer.valueOf(persona.getDiasTrabajados()));
            data.setCantidadElementos(lineaElemento);
            data.setElementoSuperior(5);
            data.setNumeroViajesMaximosPorDia(5);
            dataService.save(data);
            return personaService.find(cedula.trim()) ;
        }catch (Exception e){
            System.out.println(e);
        }
            return personaService.find(cedula);
    }

    @GetMapping("/getallfiles")
    public ResponseEntity<List<String>> getListFiles(Model model) {
        fileNames
                .stream().map(fileName -> MvcUriComponentsBuilder
                        .fromMethodName(FileController.class, "getFile", fileName).build().toString())
                .collect(Collectors.toList());

        return ResponseEntity.ok().body(fileNames);
    }

    @GetMapping(path = "/personas", produces = "application/json;charset=UTF-8")
    public List<Persona> getPersonas(Persona persona) {
        return personaService.findAll();
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = (Resource) storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName()+ "\"")
                .body(file);
    }
}