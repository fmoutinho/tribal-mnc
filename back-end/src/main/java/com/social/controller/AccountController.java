package com.social.controller;

import java.security.Principal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.social.dao.DataRepository;
import com.social.dao.RestauranteRepositorio;
import com.social.entities.Persona;
import com.social.entities.Restaurante;
import com.social.services.PersonaService;
import com.social.services.RestauranteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.social.services.UserService;
import com.social.util.CustomErrorType;
import com.social.entities.User;

/**
 * @author Fernando Moutinho
 *
 */
@RestController
@RequestMapping("account")
public class AccountController {

	public static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private UserService userService;

	@Autowired
	PersonaService personaService;
	@Autowired
	DataRepository dataRepository;

	@Autowired
	RestauranteService restauranteService;

	@Autowired
	RestauranteRepositorio restauranteRepositorio;

	// request method to create a new account by a guest
	@CrossOrigin
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User newUser) {
		if (userService.find(newUser.getUsername()) != null) {
			logger.error("Este usuario ya existe " + newUser.getUsername());
			return new ResponseEntity(
					new CustomErrorType("El Usuario : " + newUser.getUsername() + "Ya se encuentra registrado en Base de datos "),
					HttpStatus.CONFLICT);
		}
		newUser.setRole("ADMINISTRADOR");
		newUser.setFullName(newUser.getFullName());
		
		return new ResponseEntity<User>(userService.save(newUser), HttpStatus.CREATED);
	}

	@CrossOrigin
	@RequestMapping(value = "/restaurante", method = RequestMethod.POST)
	public ResponseEntity<?> createRestaurante(@RequestBody Restaurante restaurante) {
		if (restauranteService.find(restaurante.getId()) != null) {
			logger.error("Este Restaurante ya existe " + restaurante.getRazon_social());
			return new ResponseEntity(
					new CustomErrorType("El Restaurante : " + restaurante.getRazon_social() + "Ya se encuentra registrado en Base de datos "),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Restaurante>(restauranteService.save(restaurante), HttpStatus.CREATED);
	}
	@CrossOrigin
	@RequestMapping(value = "/restaurante", method = RequestMethod.PUT)
	public ResponseEntity<?> updateRestaurante(@RequestBody Restaurante restaurante) {
		if (restauranteService.find(restaurante.getId()) != null) {
			return new ResponseEntity<Restaurante>(restauranteService.update(restaurante), HttpStatus.ACCEPTED);
		}
		return new ResponseEntity(
				new CustomErrorType("Restaurante no actualizado " + restaurante.getRazon_social() + " No se encontraron datos en base de datos "),
				HttpStatus.CONFLICT);
	}

	@CrossOrigin
	@RequestMapping(value = "/restaurantes", method = RequestMethod.GET)
	public ResponseEntity<?> listadoRestaurante() {
		return new ResponseEntity(restauranteService.findAll(),
				HttpStatus.ACCEPTED);
	}

	@CrossOrigin
	@RequestMapping(value = "/restaurante/", method = RequestMethod.GET)
	public ResponseEntity<?> detalleRestaurante(@RequestParam(value = "id", required = false) long id) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		Restaurante restaurante = restauranteService.find(id);
		String restauranteJson = objectMapper.writeValueAsString(restaurante);
		return new ResponseEntity(restauranteJson,
				HttpStatus.ACCEPTED);
	}


	@CrossOrigin
	@RequestMapping(value = "profile/actividades", method = RequestMethod.POST)
	public ResponseEntity<?> createActividad(@RequestBody Persona persona) {
		if (personaService.find(persona.getCedula()) != null) {
			logger.error("Persona ya existe " + persona.getNombres());
			return new ResponseEntity(
					new CustomErrorType("Persona ya existe " + persona.getNombres() + "ya esta en Bases de datos "),
					HttpStatus.CONFLICT);
		}
		return new ResponseEntity<Persona>(personaService.save(persona), HttpStatus.CREATED);
	}



	// this is the login api/service
	@CrossOrigin
	@RequestMapping("/login")
	public Principal user(Principal principal) {
		logger.info("user logged "+principal);
		return principal;
	}

	
	
}
