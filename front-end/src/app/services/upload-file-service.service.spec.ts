import { TestBed, inject } from '@angular/core/testing';

import { UploadFileService } from './upload-file-service.service';

describe('UploadFileServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadFileService]
    });
  });

  it('should be created', inject([UploadFileService], (service: UploadFileService) => {
    expect(service).toBeTruthy();
  }));
});
