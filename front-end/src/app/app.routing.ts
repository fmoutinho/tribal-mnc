import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import {ProfileComponent} from "./components/profile/profile.component";
import {ActividadesComponent} from "./components/actividades/actividades.component";
import {UrlPermission} from "./urlPermission/url.permission";
import {VideoOperacionalComponent} from "./components/video-operacional/video-operacional.component";
import { YoutubeComponent } from './youtube/youtube.component';
import {ListUploadComponent} from './components/list-upload-component/list-upload-component.component';
import { DetailsUploadComponent } from './components/details-upload-component/details-upload-component.component';
import {PersonaComponent} from './components/persona/persona.component';
import { ResturanteListComponent} from './components/list-restaurante/resturante-list.component';
import {ResturanteRegistreComponent} from './components/registre-restaurante/resturante-registre.component'
import { ResturanteDetalleComponent} from './components/detalle-restaurante/resturante-detalle.component'

const appRoutes: Routes = [
  { path: 'profile', component: ProfileComponent ,canActivate: [UrlPermission] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'actividades', component: ActividadesComponent },
  { path: 'actividades/:cedula', component: ActividadesComponent },
  { path: 'videooperacional', component: YoutubeComponent },
  { path: 'getallfiles', component: ListUploadComponent },
  { path: 'detalleArchivo', component: DetailsUploadComponent },
  { path: 'personas', component: PersonaComponent },
  { path: 'restaurantes',component: ResturanteListComponent },
  { path: 'restaurante',component: ResturanteRegistreComponent },
  { path: 'detalleRestaurante/:id',component: ResturanteDetalleComponent },
  // otherwise redirect to profile
 { path: '**', redirectTo: '/login' }
];

export const routing = RouterModule.forRoot(appRoutes);
