import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoOperacionalComponent } from './video-operacional.component';

describe('VideoOperacionalComponent', () => {
  let component: VideoOperacionalComponent;
  let fixture: ComponentFixture<VideoOperacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoOperacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoOperacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
