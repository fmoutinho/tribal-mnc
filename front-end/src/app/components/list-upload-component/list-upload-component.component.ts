import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UploadFileService } from '../../services/upload-file-service.service';
 
@Component({
  selector: 'list-upload',
  templateUrl: './list-upload-component.component.html',
  styleUrls: ['./list-upload-component.component.css']
})
export class ListUploadComponent implements OnInit {
 
  showFile = false;
  fileUploads: Observable<string[]>;
 
  constructor(private uploadService: UploadFileService) {
    this.showFiles();
   }
 
  ngOnInit() {
  }
 
  showFiles() {
    //this.showFile = enable;
 
   // if (enable) {
      this.fileUploads = this.uploadService.getFiles();
      console.log( this.fileUploads );
   // }
  }
}