import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {AccountService} from "../../services/account.service";
import { Persona } from '../../model/model.persona';
import {AppComponent} from "../../app.component";
import { NgForm, FormControl  } from '@angular/forms';
import {UploadFileService} from "../../services/upload-file-service.service";
import {  HttpResponse, HttpEventType,HttpClientModule  } from '@angular/common/http';
import { DataTableResource } from 'angular-4-data-table-bootstrap-4';
import { User } from '../../model/model.user';

//import { DataTableResourceCustom } from './data-table-resources-custom';
@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [UploadFileService,AccountService],
})
export class ActividadesComponent implements OnInit {
  public errorMessage: string;
  persona: Persona = new Persona();
  currentUser:User;
  currenPersona :Persona;
  itemResource :any;
  //itemResource = new DataTableResource(this.jsonCompleto.toString);
 
  selectedFiles: FileList;
  currentFileUpload: File;
  constructor(private http:Http,public accountService: AccountService,private uploadService: UploadFileService) {
    this.currenPersona = JSON.parse(localStorage.getItem('currenPersona'));
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.getPersona(this.currenPersona);
    console.log(this.currenPersona);
    
    
    const myObjStr = JSON.stringify(this.currenPersona);
    console.log('Valor convertido en JSON:',myObjStr);
    console.log('json convertido:',JSON.parse(myObjStr));
    //this.itemResource = new DataTableResource();

   }

  ngOnInit() {
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
   
  }
  upload() {
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload,this.currenPersona.cedula).subscribe(event => {
     if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!',HttpResponse.name);
        alert('File is completely uploaded!');
      }
    });
    this.selectedFiles = undefined;
  }

  getPersona(personas:Persona) { 
    // CONSTRUYE URL GET A WEB SERVICE

    return this.accountService.getPersonas(personas,this.currentUser).subscribe(
      resp => {
        var response = resp.json();
      }
      , err => console.error(err)
      , //() => console.log('Prueba completada')
    );
  }
}
