import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {User} from "../../model/model.user";
import {Router} from "@angular/router";
import * as XLSX from 'ts-xlsx';
import {UploadFileService} from "../../services/upload-file-service.service";
import { HttpClient, HttpResponse, HttpEventType,HttpClientModule  } from '@angular/common/http';
import { Persona } from '../../model/model.persona';
import {AccountService} from "../../services/account.service";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [UploadFileService],
  
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  persona: Persona = new Persona();
  errorMessage: string;
  selectedFiles: FileList;
   currentFileUpload: File;
  currentUser: User;
  cedulaPersona:any;
  arrayBuffer:any;
  file:File;
  constructor(public accountService: AccountService,public authService: AuthService, public router: Router,private uploadService: UploadFileService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.cedulaPersona = JSON.parse(localStorage.getItem('cedulaPersona'));

  }
  

  ngOnInit() {
  }
  ncomingfile(event) 
  {
  this.file= event.target.files[0]; 
  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
   
  }
  /*upload() {
    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
     if (event instanceof HttpResponse) {
        console.log('File is completely uploaded!',HttpResponse.name);
      }
    });
    this.selectedFiles = undefined;
  }*/

  register() {
      this.accountService.createPersonaActividad(this.persona).subscribe(data => {
        this.router.navigate(['/actividades/',this.persona.cedula]);
      }, err => {
        console.log(err);
        alert(err);
        this.errorMessage = "Persona Existe already exist";
      }
    )
  }

// login out from the app
  logOut() {
    this.authService.logOut()
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {

        });
  }
}
