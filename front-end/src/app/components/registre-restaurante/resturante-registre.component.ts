import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AccountService} from "../../services/account.service";
import { Restaurante } from '../../model/model.registre';
import {Router} from "@angular/router";


@Component({
  selector: 'app-resturante-registre',
  templateUrl: './resturante-registre.component.html',
  styleUrls: ['./resturante-registre.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResturanteRegistreComponent implements OnInit {
  errorMessage: string;
  registre: Restaurante = new Restaurante();
  
  constructor(public accountService: AccountService, public router: Router) {
  }

  ngOnInit() {
  }

  register() {
    this.accountService.createRestaurante(this.registre).subscribe(data => {
        this.router.navigate(['/restaurante']);
      }, err => {
        console.log(err);
        alert(err);
        this.errorMessage = "Restaurante Ya existe ";
        
      }
    )
  }
}
