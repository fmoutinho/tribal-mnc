import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResturanteRegistreComponent } from './resturante-registre.component';

describe('ResturanteRegistreComponent', () => {
  let component: ResturanteRegistreComponent;
  let fixture: ComponentFixture<ResturanteRegistreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResturanteRegistreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResturanteRegistreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
