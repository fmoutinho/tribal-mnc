import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AccountService} from "../../services/account.service";
import { Observable } from 'rxjs';
import { Persona } from '../../model/model.persona';
import {Router} from '@angular/router';

@Component({
  selector: 'app-resturante-list',
  templateUrl: './resturante-list.component.html',
  styleUrls: ['./resturante-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResturanteListComponent implements OnInit {
  restaurante:any;
  arregloRestaurante:[];
  itemResource:any;
  items = [];
  itemCount = 0;
  constructor(public accountService: AccountService,private router:Router) {
    this.getRestaurantes();
   }

  ngOnInit() {
  }
  myFn(id){
    console.log("Vamos a mostrar el id del Restaurante:"+id);
    this.router.navigate(['detalleRestaurante/'+id]);

  }
  getRestaurantes() {
    //this.showFile = enable;
    let i=0;
   // if (enable) {
      this.restaurante = this.accountService.getRestaurantes().subscribe(
      
        resp => {
          //this.itemResource =  new DataTableResourceCustom(resp);
          let response  =resp.json();
          this.arregloRestaurante=  response;
        }
        , err => console.error(err)
        , //() => console.log('Prueba completada')
      );
     
    };

}

  

