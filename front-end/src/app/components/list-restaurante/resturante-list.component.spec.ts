import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResturanteListComponent } from './resturante-list.component';

describe('PersonaComponent', () => {
  let component: ResturanteListComponent;
  let fixture: ComponentFixture<ResturanteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResturanteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResturanteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
