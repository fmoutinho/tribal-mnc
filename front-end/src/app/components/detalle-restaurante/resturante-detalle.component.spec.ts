import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResturanteDetalleComponent } from './resturante-detalle.component';

describe('ResturanteDetalleComponent', () => {
  let component: ResturanteDetalleComponent;
  let fixture: ComponentFixture<ResturanteDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResturanteDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResturanteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
