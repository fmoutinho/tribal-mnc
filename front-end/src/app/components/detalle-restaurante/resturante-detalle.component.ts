import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AccountService} from "../../services/account.service";
import {ActivatedRoute, Router} from '@angular/router';
import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";

@Component({
  selector: 'app-resturante-detalle',
  templateUrl: './resturante-detalle.component.html',
  styleUrls: ['./resturante-detalle.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ResturanteDetalleComponent implements OnInit {
  [x: string]: any;
  restaurante:any;
  arregloRestaurante:any ;
  itemResource:any;
  items = [];
  itemCount = 0;
  idRestaurante :any;
  razonSocial:String;
  contact: Array<Object>;
  rif:any;
  ubicacion:any;
  comentario:any;

  constructor(public accountService: AccountService,private router:Router,private _route: ActivatedRoute,private _http: HttpClient) {
    this.idRestaurante = this._route.snapshot.paramMap.get('id');
    this.getDetalleRestaurantes(this.idRestaurante);
   }

  ngOnInit() {
  }
  getDetalleRestaurantes(id) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    let params = new URLSearchParams();
    params.append("id", this.idRestaurante )  
   // if (enable) {

      this.restaurante = this.accountService.getDetalleRestaurantes(id).subscribe(
        resp => {
          this.contact = [];

          //this.itemResource =  new DataTableResourceCustom(resp);
          //let response  =resp.json();
          this.arregloRestaurante=  resp.json();
          //this.contact = Object.values(this.arregloRestaurante)
          console.log("Arreglo de Restaurante:",this.arregloRestaurante)
          this.idRestaurante = this.arregloRestaurante.id;
          this.razonSocial = this.arregloRestaurante.razon_social;
          this.ubicacion = this.arregloRestaurante.ubicacion;
          this.rif = this.arregloRestaurante.rif;
          this.comentario = this.arregloRestaurante.comentarios;

        }
        , err => console.error(err)
        , //() => console.log('Prueba completada')
      );
     
    };

   

}

  

