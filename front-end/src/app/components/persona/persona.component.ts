import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AccountService} from "../../services/account.service";
import { DataTableResourceCustom } from './data-table-resources-custom';
import { Observable } from 'rxjs';
import { Persona } from '../../model/model.persona';
@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PersonaComponent implements OnInit {
  persona:any;
  arregloPersona:[];
  itemResource:any;
  items = [];
  itemCount = 0;
  constructor(public accountService: AccountService) {
    this.getPersonas();
   }

  ngOnInit() {
  }
  getPersonas() {
    //this.showFile = enable;
    let i=0;
   // if (enable) {
      this.persona = this.accountService.getPersona().subscribe(
      
        resp => {
          //this.itemResource =  new DataTableResourceCustom(resp);
          let response  =resp.json();
          this.arregloPersona=  response;
          console.log("DATA PARA MOSTRAR DE LAS PERSONAS :",this.arregloPersona);
          
        //  this.itemResource.count().then(count => this.itemCount = count);
         
        }
        , err => console.error(err)
        , //() => console.log('Prueba completada')
      );

     
    };

   
 

// special properties:

onclick(rowEvent) {
    console.log('Clicked: ' + rowEvent.row.item.name);
}

rowDoubleClick(rowEvent) {
    alert('Double clicked: ' + rowEvent.row.item.name);
}

rowTooltip(item) { return item.jobTitle; }
}

  

